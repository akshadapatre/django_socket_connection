from configparser import ConfigParser
import socket
import sys
import traceback
import errno
from threading import Thread
from datetime import datetime
from pytz import timezone

class server:
    def read_config(self):
        # read the config file and get the values
        tcp_config = ConfigParser()
        tcp_config.read('config.ini')
        tcp_host = tcp_config['tcp_server']['host']
        tcp_port = tcp_config['tcp_server']['port']
        tcp_file_name = tcp_config['tcp_client']['log_file_name']
        print("Host : ", tcp_host, " Port : ", tcp_port)
        return tcp_host, tcp_port ,tcp_file_name





            


    def run_server(self):
        tcp_host, tcp_port, tcp_file_name = server.read_config(self)
        print(f'{tcp_host} {tcp_port} {tcp_file_name}')
        try:
            soc_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            soc_tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            print("Socket successfully created")
        except socket.error as err:
            print("Socket creation failed with error %s" % err)
            print('This is a test')
            sys.exit()


        try:
            soc_tcp.bind((tcp_host, int(tcp_port)))

            print("Binding to host and port ")
        except:
            print("bind failed. Error : " + str(sys.exc_info()))
            sys.exit()

        try:
            soc_tcp.listen()
            print("listening on the socket")
        except:
            print("listening failed. Error : " + str(sys.exc_info()))
            sys.exit()
        while True:
            try:
                tcp_conn, address = soc_tcp.accept() #connection , address array
                ip, port = str(address[0]), str(address[1])
                time = datetime.now(timezone('UTC'))
                print("Connected with " + ip + ":" + port)
                Thread(target=threadwork, args=(tcp_conn,ip,port,time)).start()

            except:
                print("connection failed")
                traceback.print_exc()

        tcp_conn.close()
        #print('connection closed')

def threadwork(tcp_conn,ip,port,time):
    client_msg = tcp_conn.recv(4000)

    client_msg = 'pranam' + client_msg.decode('utf-8') + ' connected with :' + ip + ' port' + port + ' at:' + str(time)
    print('new msg>', client_msg)
    tcp_conn.send(client_msg.encode('utf-8'))
t1 = server()
t1.run_server()
