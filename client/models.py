from django.db import models
from django.db.models import Model

# Create your models here.
class tblCommandAudit(models.Model):
    # Name of the model in the Database
    sender_user = models.TextField()
    sender_msg = models.TextField()
    sent_date_time = models.DateTimeField()
    sender_ip_address = models.GenericIPAddressField()
    response_msg = models.TextField()
    response_date_time = models.DateTimeField()
    #msg_content = models.TextField()
    server_ip_address = models.GenericIPAddressField()
    server_port = models.IntegerField()

    duration_between_send_receive_msg = models.CharField(max_length=255)


    class Meta:
        db_table ='tblCommandAudit'

