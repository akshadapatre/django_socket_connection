from rest_framework import serializers
from .models import *

class tblcommandauditserializers(serializers.ModelSerializer):
    class Meta:
        model=tblCommandAudit
        fields="__all__"