from configparser import ConfigParser
import socket
import sys
import traceback

class client:
    def read_config(self):
        '''
        # read the config file and get the values
        tcp_config = ConfigParser()
        tcp_config.read('config.ini')
        tcp_host = tcp_config['tcp_client']['host']
        tcp_port = tcp_config['tcp_client']['port']
        tcp_file_name = tcp_config['tcp_client']['log_file_name']
        print("Host : ", tcp_host, " Port : ", tcp_port)
        return tcp_host, tcp_port, tcp_file_name
        '''
    def send_msg(self,soc_tcp):
        serverinput = 'this is msg'
        soc_tcp.send(serverinput.encode('utf-8'))
    def recv_msg(self,soc_tcp):
        client_input = soc_tcp.recv(4000)
        return client_input
    def run_client(self):
        tcp_host, tcp_port = '132.145.63.232','9511'
        print(f'{tcp_host} {tcp_port}')
        try:
            soc_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            soc_tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            print("Socket successfully created")
        except socket.error as err:
            print("Socket creation failed with error %s" % err)
            sys.exit()
        # Try to bind the socket and port
        try:
            soc_tcp.connect((tcp_host, int(tcp_port)))
            print("connecting to host and port ")
        except:
            print("connection failed. Error : " + str(sys.exc_info()))
            sys.exit()
        #while True:
            #client.send_msg(self,soc_tcp)
            #client_msg = client.recv_msg(self,soc_tcp)
            #print(f'received msg>{client_msg.decode("utf-8")}')
#t1= client()
#t1.run_client()